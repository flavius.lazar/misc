/*
	n! = a * 10 ^ b, b=?
*/

#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>

#define NRELEMS(x) (sizeof(x) / sizeof((x)[0]))

typedef struct {
	int exp2;
	int exp5;
} exponent_t;

typedef struct {
	unsigned int low;
	unsigned int high;
} interval_t;

void prime_factors(int n, exponent_t* exp_n) {
	while (n % 10 == 0) {
		exp_n->exp2++;
		exp_n->exp5++;
		n /= 10;
	}

	while (n % 5 == 0) {
		exp_n->exp5++;
		n /= 5;
	}

	while (n % 2 == 0) {
		exp_n->exp2++;
		n /= 2;
	}
}

int zeroos(unsigned int n) {
	int zeros = 0;
	exponent_t exp;

	if (n == 0 || n == 1) {
		zeros = 0;
	}
	else {
		exp.exp2 = exp.exp5 = 0;
		for (unsigned int i = 2; i <= n; ++i) {
			prime_factors(i, &exp);
		}

		zeros = (exp.exp2 < exp.exp5 ? exp.exp2 : exp.exp5);
	}
	
	return zeros;
}

double time_diff(struct timespec start, struct timespec end) {
	return (end.tv_sec - start.tv_sec) + (end.tv_nsec - start.tv_nsec) / 1e9;
}

void run_single_core(unsigned int *input_numbers, int n) {
	struct timespec start_time, end_time;

	for (int i = 0; i < n; ++i) {
		clock_gettime(CLOCK_REALTIME, &start_time);
		int zeros = zeroos(input_numbers[i]);
		clock_gettime(CLOCK_REALTIME, &end_time);
		double run_time = time_diff(start_time, end_time);
		printf("%d! ends with %d zeros and took %fs\n", input_numbers[i], zeros, run_time);
	}
}

void *zeroos_run(void *input) {
	interval_t *interval = (interval_t*) input;
	exponent_t *exp = (exponent_t*) malloc(sizeof(exponent_t));
	exp->exp2 = exp->exp5 = 0;

	for (int i = interval->low; i <= interval->high; ++i) {
		prime_factors(i, exp);
	}

	return (void*) exp;
}

int split_work(unsigned int n, int cpus) {
	interval_t *intervals = (interval_t*) malloc(sizeof(interval_t) * cpus);
	pthread_t *threads = (pthread_t*) malloc(sizeof(pthread_t) * cpus);
	unsigned int slice = n / cpus;

	for (int i = 0; i < cpus + 1; ++i) {
		intervals[i].low = i * slice + 1;
		intervals[i].high = (i + 1) * slice;
		if (i == cpus) {
			// n is the limit
			intervals[i].high = n;			
		}
		// printf("%d ->[%d, %d]\n", n, intervals[i].low, intervals[i].high);
		pthread_create(&threads[i], NULL, zeroos_run, &intervals[i]);
	}

	exponent_t *exp;
	exponent_t exp_total;
	void *ret;
	exp_total.exp2 = exp_total.exp5 = 0;
	for (int i = 0; i < cpus + 1; ++i) {
		pthread_join(threads[i], &ret);
		exp = (exponent_t*) ret;
		exp_total.exp2 += exp->exp2;
		exp_total.exp5 += exp->exp5;
		free(exp);
	}

	free(intervals);
	free(threads);

	return (exp_total.exp2 < exp_total.exp5 ? exp_total.exp2 : exp_total.exp5);
}

void run_multiple_cores(unsigned int *input_numbers, int n) {
	struct timespec start_time, end_time;
	int cpus = sysconf(_SC_NPROCESSORS_ONLN);
	printf("Number of available cpu(s)/core(s): %d\n", cpus);
	
	
	for (int i = 0; i < n; ++i) {
		clock_gettime(CLOCK_REALTIME, &start_time);
		int zeros = split_work(input_numbers[i], cpus);
		clock_gettime(CLOCK_REALTIME, &end_time);
		double run_time = time_diff(start_time, end_time);
		printf("%d! ends with %d zeros and took %fs\n", input_numbers[i], zeros, run_time);
	}
}

int main(int argc, char **argv) {
	unsigned int a[] = { 120, 456, 345678, 394839483, 37438249, 567657657, 858758574};

	printf("Results on single core:\n");
	run_single_core(a, NRELEMS(a));
	printf("\nResults on multiple cpu(s)/core(s):\n");
	run_multiple_cores(a, NRELEMS(a));

	return 0;
}